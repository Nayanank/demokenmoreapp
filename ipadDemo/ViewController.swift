//
//  ViewController.swift
//  ipadDemo
//
//  Created by Mahajan, Nayanank (Contractor) on 4/24/17.
//  Copyright © 2017 Mahajan, Nayanank (Contractor). All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var refrigTemperatureLabel: UILabel!
    
    @IBOutlet weak var freezerTemperature: UILabel!
    
    var refrigVal = ""
    @IBOutlet weak var washerStateLabel: UILabel!
    @IBOutlet weak var dryerStateLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(refrigVal == "") {
            print("empty refrigVal")
            refrigTemperatureLabel.text = HomePageData.sharedInstance.getRefrigeratorTemperature()
        } else {
            print("value of refrigVal inside else :: " , refrigVal)
            refrigTemperatureLabel.text = refrigVal;
        }
        
        if(HomePageData.sharedInstance.getWasherState() == "Ready") {
                washerStateLabel.text = "Remote On"
        }
        else if(HomePageData.sharedInstance.getWasherState() == "Running") {
            washerStateLabel.text = HomePageData.sharedInstance.getWasherTimeRemaining() +  " Washing"
        }
        else if(HomePageData.sharedInstance.getWasherState() == "Paused") {
            washerStateLabel.text = "Paused"
        }
        
        //Freezer temp
        freezerTemperature.text = HomePageData.sharedInstance.getFreezerTemperature()
        
        //Dryer
//        print("Dryer state :: \(HomePageData.sharedInstance.getDryerState())")
        if(HomePageData.sharedInstance.getDryerState() == "Ready") {
            dryerStateLabel.text = "Remote On"
        } else if(HomePageData.sharedInstance.getDryerState() == "Running") {
            dryerStateLabel.text = "0:20 Drying"
        } else if(HomePageData.sharedInstance.getDryerState() == "Paused") {
            dryerStateLabel.text = "Paused"
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func tap(_ sender: UITapGestureRecognizer) {
       let appdelegate = UIApplication.shared.delegate as! AppDelegate
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let refrigeratorPDPViewController = storyboard?.instantiateViewController(withIdentifier: "RefrigeratorPDPViewController") as! RefrigeratorPDPViewController
        refrigeratorPDPViewController.refrigVal = refrigTemperatureLabel.text!
        let nav = UINavigationController(rootViewController: refrigeratorPDPViewController)
        appdelegate.window!.rootViewController = nav
    }

    @IBAction func washerTap(_ sender: UITapGestureRecognizer) {
        
        print("Washer Tapped")
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        if(HomePageData.sharedInstance.getWasherState() == "Ready") {
            let washerPDPViewController = storyboard?.instantiateViewController(withIdentifier: "WasherPDPViewController") as! WasherPDPViewController
            self.navigationController?.pushViewController(washerPDPViewController, animated: false)
            let nav = UINavigationController(rootViewController: washerPDPViewController)
            appdelegate.window!.rootViewController = nav
        } else if(HomePageData.sharedInstance.getWasherState() == "Running") {
            let washerRunningViewController = storyboard?.instantiateViewController(withIdentifier: "WasherRunningViewController") as! WasherRunningViewController
            self.navigationController?.pushViewController(washerRunningViewController, animated: false)
            let nav = UINavigationController(rootViewController: washerRunningViewController)
            appdelegate.window!.rootViewController = nav
        } else if(HomePageData.sharedInstance.getWasherState() == "Paused") {
            let washerPausedViewController = storyboard?.instantiateViewController(withIdentifier: "WasherPausedViewController") as! WasherPausedViewController
            self.navigationController?.pushViewController(washerPausedViewController, animated: false)
            let nav = UINavigationController(rootViewController: washerPausedViewController)
            appdelegate.window!.rootViewController = nav
        }
        
        
        
    }
    
    
    @IBAction func dryerTap(_ sender: UITapGestureRecognizer) {
        
        print("Dryer state on tap :: \(HomePageData.sharedInstance.getDryerState())");
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if(HomePageData.sharedInstance.getDryerState() == "Ready") {
            let dryerReadyViewController = storyboard?.instantiateViewController(withIdentifier: "DryerReadyViewController") as! DryerReadyViewController
            self.navigationController?.pushViewController(dryerReadyViewController, animated: false)
            let nav = UINavigationController(rootViewController: dryerReadyViewController)
            appdelegate.window!.rootViewController = nav
        } else if(HomePageData.sharedInstance.getDryerState() == "Running") {
            let dryerRunningViewController = storyboard?.instantiateViewController(withIdentifier: "DryerRunningViewController") as! DryerRunningViewController
            self.navigationController?.pushViewController(dryerRunningViewController, animated: false)
            let nav = UINavigationController(rootViewController: dryerRunningViewController)
            appdelegate.window!.rootViewController = nav
        } else if(HomePageData.sharedInstance.getDryerState() == "Paused") {
            let dryerPausedViewController = storyboard?.instantiateViewController(withIdentifier: "DryerPausedViewController") as! DryerPausedViewController
            self.navigationController?.pushViewController(dryerPausedViewController, animated: false)
            let nav = UINavigationController(rootViewController: dryerPausedViewController)
            appdelegate.window!.rootViewController = nav
        }
    }
    
    
    
}


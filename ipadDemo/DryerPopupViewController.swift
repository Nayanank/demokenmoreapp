//
//  DryerPopupViewController.swift
//  ipadDemo
//
//  Created by Mahajan, Nayanank (Contractor) on 5/7/17.
//  Copyright © 2017 Mahajan, Nayanank (Contractor). All rights reserved.
//

import UIKit

class DryerPopupViewController: UIViewController {
    @IBOutlet weak var settingsImageView: UIImageView!
    @IBOutlet weak var prodInfoAndGuidesImageView: UIImageView!

    @IBOutlet weak var usageHistoryImageView: UIImageView!
    
    @IBOutlet weak var myCycleImageView: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settingsImageView.image = #imageLiteral(resourceName: "icon_menu_settings")
        usageHistoryImageView.image = #imageLiteral(resourceName: "icon_menu_usage")
        prodInfoAndGuidesImageView.image = #imageLiteral(resourceName: "icon_menu_info")
        myCycleImageView.image = #imageLiteral(resourceName: "icon_offline_pdp")
        // Do any additional setup after loading the view.
    }

   
    @IBAction func clickDismiss(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func cancelButtonClick(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
}

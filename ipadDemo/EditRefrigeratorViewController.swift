//
//  EditRefrigeratorViewController.swift
//  ipadDemo
//
//  Created by Mahajan, Nayanank (Contractor) on 4/30/17.
//  Copyright © 2017 Mahajan, Nayanank (Contractor). All rights reserved.
//

import UIKit

class EditRefrigeratorViewController: UIViewController {

    var refrigVal = ""
    @IBOutlet weak var refrigeratorSlider: UISlider!
    @IBOutlet weak var freezerSlider: UISlider!
    @IBOutlet weak var refigeratorTempLabel: UILabel!
    
    @IBOutlet weak var freezerTempLabel: UILabel!
    @IBOutlet weak var refrigeratorDialView: RefrigeratorDial!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("HPD on EditVC :: " + HomePageData.sharedInstance.getRefrigeratorTemperature())
        if(refrigVal == "") {
            refigeratorTempLabel.text = HomePageData.sharedInstance.getRefrigeratorTemperature()
        } else {
            refigeratorTempLabel.text = refrigVal
        }
        refrigeratorDialView.sliderVal = Int(refigeratorTempLabel.text!)!
        refrigeratorDialView.setNeedsLayout();
        
        refrigeratorSlider.setMinimumTrackImage(UIImage(), for: .normal)
        refrigeratorSlider.setMaximumTrackImage(UIImage(), for: .normal)
        refrigeratorSlider.setThumbImage(UIImage(), for: .normal)
        
        freezerSlider.setMinimumTrackImage(UIImage(), for: .normal)
        freezerSlider.setMaximumTrackImage(UIImage(), for: .normal)
        freezerSlider.setThumbImage(UIImage(), for: .normal)
        
        freezerTempLabel.text = HomePageData.sharedInstance.getFreezerTemperature()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closePopupClicked(_ sender: UIButton) {
        self.performSegue(withIdentifier: "closeChangeTemperaturePopup", sender: self)
    }

    
    @IBAction func refrigeratorTempChanged(_ sender: UISlider) {
        let currentValue = Int(sender.value);
        refigeratorTempLabel.text = String(currentValue)
        refrigeratorDialView.sliderVal = currentValue
        refrigeratorDialView.setNeedsDisplay();
    }
    
    @IBAction func saveClicked(_ sender: UIButton) {
        
        HomePageData.sharedInstance.setRefrigeratorTemperature(refrigeratorTemperature: refigeratorTempLabel.text!)
        HomePageData.sharedInstance.setFreezerTemperature(freezerTemperature: freezerTempLabel.text!)
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let refrigeratorPDPViewController = storyboard?.instantiateViewController(withIdentifier: "RefrigeratorPDPViewController") as! RefrigeratorPDPViewController
        refrigeratorPDPViewController.refrigVal = refigeratorTempLabel.text!
        let nav = UINavigationController(rootViewController: refrigeratorPDPViewController)
        appdelegate.window!.rootViewController = nav
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "closeChangeTemperaturePopup" {
            let destination = segue.destination as! RefrigeratorPDPViewController
            destination.refrigVal = refrigVal
        }
    }
    
    @IBAction func refrigeratorValueTapped(_ sender: UITapGestureRecognizer) {
        
        if let slider = sender.view as? UISlider {
            if slider.isHighlighted { return }
            let point = sender.location(in: slider)
            let percentage = Float(point.x / slider.bounds.width)
            let delta = percentage * (slider.maximumValue - slider.minimumValue)
            let value = slider.minimumValue + delta
            slider.setValue(value, animated: true)
            print("refrigeratorValueChanged :: ", String(value))
            
            refigeratorTempLabel.text = String(Int(value))
            
            refrigeratorDialView.sliderVal = Int(value)
            
            refrigeratorDialView.setNeedsDisplay();
        }
    }
    @IBAction func freezerTempChanged(_ sender: UISlider) {
        let currentValue = Int(sender.value);
        freezerTempLabel.text = String(currentValue)
        print("freezer vale :: \(sender.value)")
        refrigeratorDialView.freezerVal = currentValue
        refrigeratorDialView.setNeedsDisplay()
    }
    
    @IBAction func freezerValueTapped(_ sender: UITapGestureRecognizer) {
        
        if let slider = sender.view as? UISlider {
            if slider.isHighlighted { return }
            let point = sender.location(in: slider)
            let percentage = Float(point.x / slider.bounds.width)
            let delta = percentage * (slider.maximumValue - slider.minimumValue)
            let value = slider.minimumValue + delta
            slider.setValue(value, animated: true)
            print("freezer :: ", String(value))
            
            freezerTempLabel.text = String(Int(value))
            
            refrigeratorDialView.freezerVal = Int(value)
            
            refrigeratorDialView.setNeedsDisplay();
    }
    
}
}

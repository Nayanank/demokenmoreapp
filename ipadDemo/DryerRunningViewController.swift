//
//  DryerRunningViewController.swift
//  ipadDemo
//
//  Created by Mahajan, Nayanank (Contractor) on 5/6/17.
//  Copyright © 2017 Mahajan, Nayanank (Contractor). All rights reserved.
//

import UIKit

class DryerRunningViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        HomePageData.sharedInstance.setDryerState(dryerState: "Running")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func offButtonClicked(_ sender: Any) {
       HomePageData.sharedInstance.setDryerState(dryerState: "Ready") 
    }

}

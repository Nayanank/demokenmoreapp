//
//  RefrigeratorPDPViewController.swift
//  ipadDemo
//
//  Created by Mahajan, Nayanank (Contractor) on 4/30/17.
//  Copyright © 2017 Mahajan, Nayanank (Contractor). All rights reserved.
//

import UIKit

class RefrigeratorPDPViewController: UIViewController {
    
    var refrigVal = ""
    @IBOutlet weak var refrigeratorDialView: RefrigeratorDial!
    @IBOutlet weak var refrigeratorTempLabel: UILabel!
    
    @IBOutlet weak var freezerTempLabel: UILabel!
    
   
    @IBOutlet weak var accelaIceImageView: UIImageView!
    @IBOutlet weak var accelaIceCheckboxImageView: UIImageView!
    
    @IBOutlet weak var maxAirFilterImageView: UIImageView!
    @IBOutlet weak var maxAirFilterCheckboxImageView: UIImageView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("HPD on PDP :: " + HomePageData.sharedInstance.getRefrigeratorTemperature())
        if(refrigVal == "") {
            refrigeratorTempLabel.text = HomePageData.sharedInstance.getRefrigeratorTemperature()
        } else {
           refrigeratorTempLabel.text = refrigVal
        }
        refrigeratorDialView.sliderVal = Int(refrigeratorTempLabel.text!)!
        refrigeratorDialView.setNeedsLayout();
        
        // Do any additional setup after loading the view.
        freezerTempLabel.text = HomePageData.sharedInstance.getFreezerTemperature()
        
        if(HomePageData.sharedInstance.isAccelaIceChecked()) {
            //checkbox is  checked right now
            accelaIceImageView.image = #imageLiteral(resourceName: "icon_accelaIce_on")
            accelaIceCheckboxImageView.image = #imageLiteral(resourceName: "checkbox_on")
        } else {
            //checkbox is not checked
            accelaIceImageView.image = #imageLiteral(resourceName: "icon_accelaIce_off")
            accelaIceCheckboxImageView.image = #imageLiteral(resourceName: "checkbox_off")
            
        }
        
        if(HomePageData.sharedInstance.isMaxAirFilterChecked()) {
            //checkbox is  checked right now
            maxAirFilterImageView.image = #imageLiteral(resourceName: "icon_maxair_on")
            maxAirFilterCheckboxImageView.image = #imageLiteral(resourceName: "checkbox_on")
        } else {
            //checkbox is not checked
            maxAirFilterImageView.image = #imageLiteral(resourceName: "icon_maxair_off")
            maxAirFilterCheckboxImageView.image = #imageLiteral(resourceName: "checkbox_off")
            
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func editRefrigerator(_ sender: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "editRefrigerator", sender: self)
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "homePageSegue" {
            let destination = segue.destination as! ViewController
            destination.refrigVal = refrigVal
        }
    }
    @IBAction func accelaIceViewTapped(_ sender: UITapGestureRecognizer) {
        print("tapped")
        if(HomePageData.sharedInstance.isAccelaIceChecked()) {
            //checkbox is  checked right now
            accelaIceImageView.image = #imageLiteral(resourceName: "icon_accelaIce_off")
            accelaIceCheckboxImageView.image = #imageLiteral(resourceName: "checkbox_off")
            HomePageData.sharedInstance.setAccelaIceChecked(accelaIceChecked: false)
        } else {
            //checkbox is not checked
            accelaIceImageView.image = #imageLiteral(resourceName: "icon_accelaIce_on")
            accelaIceCheckboxImageView.image = #imageLiteral(resourceName: "checkbox_on")
            HomePageData.sharedInstance.setAccelaIceChecked(accelaIceChecked: true)
        }

    }
    @IBAction func maxAirFilterViewTapped(_ sender: UITapGestureRecognizer) {
        print("tapped")
        if(HomePageData.sharedInstance.isMaxAirFilterChecked()) {
            //checkbox is  checked right now
            maxAirFilterImageView.image = #imageLiteral(resourceName: "icon_maxair_off")
            maxAirFilterCheckboxImageView.image = #imageLiteral(resourceName: "checkbox_off")
            HomePageData.sharedInstance.setMaxAirFilterChecked(maxAirFilterChecked: false)
        } else {
            //checkbox is not checked
            maxAirFilterImageView.image = #imageLiteral(resourceName: "icon_maxair_on")
            maxAirFilterCheckboxImageView.image = #imageLiteral(resourceName: "checkbox_on")
            HomePageData.sharedInstance.setMaxAirFilterChecked(maxAirFilterChecked: true)
        }

    }
    
    
    
}

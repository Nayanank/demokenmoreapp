//
//  WasherRunningViewController.swift
//  ipadDemo
//
//  Created by Mahajan, Nayanank (Contractor) on 5/4/17.
//  Copyright © 2017 Mahajan, Nayanank (Contractor). All rights reserved.
//

import UIKit

class WasherRunningViewController: UIViewController {
    

    @IBOutlet weak var progressBarSensing: UIView!
    @IBOutlet weak var progressBarWashing: UIView!
    
    @IBOutlet weak var timeRemaining: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        progressBarSensing.alpha = 0
        progressBarWashing.alpha = 0
        
        print("washer state :: \(HomePageData.sharedInstance.getWasherState())")
        //logic for animation
        if(HomePageData.sharedInstance.getWasherState() == "Ready") {
            //animation logic
            UIView.animate(withDuration: 5, animations: {
                self.progressBarSensing.alpha = 1
                self.timeRemaining.text = "--:--"
                print("1 :: \(HomePageData.sharedInstance.getWasherState())")
            }) {(true) in
                    UIView.animate(withDuration: 5, animations: {
                        print("2 :: \(HomePageData.sharedInstance.getWasherState())")
                        self.progressBarSensing.alpha = 0
                        self.progressBarWashing.alpha = 1
                    
                        self.timeRemaining.text = "0:26"
                        HomePageData.sharedInstance.setWasherTimeRemaining(timeRemaining: "0:26")
                        HomePageData.sharedInstance.setWasherState(washerState: "Running")
                    
                    })
                }
            }
        else if(HomePageData.sharedInstance.getWasherState() == "Running") {
            self.progressBarWashing.alpha = 1
            self.timeRemaining.text = "0:26"
            HomePageData.sharedInstance.setWasherTimeRemaining(timeRemaining: "0:26")
        } else if(HomePageData.sharedInstance.getWasherState() == "Paused") {
            HomePageData.sharedInstance.setWasherState(washerState: "Running")
            self.progressBarWashing.alpha = 1
            self.timeRemaining.text = "0:26"
            HomePageData.sharedInstance.setWasherTimeRemaining(timeRemaining: "0:26")
        }
        
        
    }

    @IBAction func pauseButtonClicked(_ sender: UIButton) {
        HomePageData.sharedInstance.setWasherState(washerState: "Paused")
        performSegue(withIdentifier: "pauseSegue", sender: self)
    }
    
    
    @IBAction func washerOff(_ sender: UIButton) {
        HomePageData.sharedInstance.setWasherState(washerState: "Ready")
    }

}

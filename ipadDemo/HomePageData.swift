//
//  HomePageData.swift
//  ipadDemo
//
//  Created by Mahajan, Nayanank (Contractor) on 4/30/17.
//  Copyright © 2017 Mahajan, Nayanank (Contractor). All rights reserved.
//

import Foundation

class HomePageData {
    
    static let sharedInstance = HomePageData()
    
    init() {
        print("HomePageData initialized")
    }
    
    private var refrigeratorTemperature = "37";
    
    private var washerState = "Ready"
    
    private var washerTimeRemaining = ""
    
    private var freezerTemperature = "0"
    
    private var accelaIceChecked = false;
    
    private var maxAirFilterChecked = false;
    
    private var dryerState = "Ready"
    
    private var dryerChimeLevel = "Off"
    
    private var washerChimeLevele = "Off"
    
    private var refIceMaker = "Off"
    
    func isAccelaIceChecked() -> Bool {
        return self.accelaIceChecked
    }
    
    func setAccelaIceChecked(accelaIceChecked : Bool) {
        self.accelaIceChecked = accelaIceChecked
    }
    
    func isMaxAirFilterChecked() -> Bool {
        return self.maxAirFilterChecked
    }
    
    func setMaxAirFilterChecked(maxAirFilterChecked : Bool) {
        self.maxAirFilterChecked = maxAirFilterChecked
    }
    
    func setRefrigeratorTemperature(refrigeratorTemperature: String) {
        self.refrigeratorTemperature = refrigeratorTemperature
    }
    
    func getRefrigeratorTemperature() -> String {
        return self.refrigeratorTemperature
    }
    
    func setFreezerTemperature(freezerTemperature: String) {
        self.freezerTemperature = freezerTemperature
    }
    
    func getFreezerTemperature() -> String {
        return self.freezerTemperature
    }
    
    func setWasherState(washerState: String) {
        self.washerState = washerState
    }
    
    func getWasherState() -> String {
        return self.washerState
    }
    
    func setWasherTimeRemaining(timeRemaining: String) {
        self.washerTimeRemaining = timeRemaining
    }
    
    func getWasherTimeRemaining() -> String {
        return self.washerTimeRemaining
    }
    
    func setDryerState(dryerState: String) {
        self.dryerState = dryerState
    }
    
    func getDryerState() -> String {
        return self.dryerState
    }
    
}

//
//  WasherPausedViewController.swift
//  ipadDemo
//
//  Created by Mahajan, Nayanank (Contractor) on 5/4/17.
//  Copyright © 2017 Mahajan, Nayanank (Contractor). All rights reserved.
//

import UIKit

class WasherPausedViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func washerPauseClick(_ sender: UIButton) {
        HomePageData.sharedInstance.setWasherState(washerState: "Ready")
    }
}

//
//  RefrigeratorDial.swift
//  ipadDemo
//
//  Created by Mahajan, Nayanank (Contractor) on 4/30/17.
//  Copyright © 2017 Mahajan, Nayanank (Contractor). All rights reserved.
//

import UIKit

class RefrigeratorDial: UIView {
    
    var sliderVal = 0;
    var freezerVal = 0;
    var freezerValChanged = false
    
    override func draw(_ rect: CGRect) {
        //circle
        
        let context = UIGraphicsGetCurrentContext()
        
        context?.setLineWidth(8.0)
        
        context?.setStrokeColor(UIColor.gray.cgColor)
        
        let rectangle1 = CGRect(x: 5,y: 5,width: 430,height: 430)
        
        context?.addEllipse(in: rectangle1)
        
        context?.strokePath()
        
        //_kenmoreBlue = [UIColor colorWithRed: 0 green: 0.549 blue: 0.8 alpha: 1];
        //_kmTurquoise = [UIColor colorWithRed: 0 green: 0.698 blue: 0.804 alpha: 1];
       // let _kenmoreBlue = UIColor(red: 0, green: 0.498, blue: 0.750, alpha: 1)
        context?.setFillColor(UIColor.lightGray.cgColor)

        
        let rectangle = CGRect(x: 10,y: 10,width: 420,height: 420)
        
        context?.addEllipse(in: rectangle)
        
        context?.strokePath()
        
        //_kenmoreBlue = [UIColor colorWithRed: 0 green: 0.549 blue: 0.8 alpha: 1];
        //_kmTurquoise = [UIColor colorWithRed: 0 green: 0.698 blue: 0.804 alpha: 1];
        let _kenmoreBlue = UIColor(red: 0, green: 0.498, blue: 0.750, alpha: 0.8)
        context?.setFillColor(_kenmoreBlue.cgColor)
        
        var inActiveColor = UIColor(white: 1, alpha: 0.5)
        
        
        
        context?.fillEllipse(in: rectangle)
        
        draw1stBox(color: inActiveColor)
        
        draw2ndBox(color: inActiveColor)
        
        draw3rdBox(color: inActiveColor)
        
        draw4thBox(color: inActiveColor)
        
        draw5thBox(color: inActiveColor)
        
        draw6thBox(color: inActiveColor)
        
        draw7thBox(color: inActiveColor)
        
        draw8thBox(color: inActiveColor)
        
        draw9thBox(color: inActiveColor)
        
        draw10thBox(color: inActiveColor)
        
        draw11thBox(color: inActiveColor)
        
        draw12thBox(color: inActiveColor)
        
        draw13thBox(color: inActiveColor)
        
        draw14thBox(color: inActiveColor)
        
        draw15thBox(color: inActiveColor)
        
        draw16thBox(color: inActiveColor)
        
        draw17thBox(color: inActiveColor)
        
        draw18thBox(color: inActiveColor)
        
        draw19thBox(color: inActiveColor)
        
        
            
        draw20thBox(color: inActiveColor)
            
        
        
        //draw20thBox(color: inActiveColor)
        
        draw21thBox(color: inActiveColor)
        
        draw22thBox(color: inActiveColor)
        
        draw23thBox(color: inActiveColor)
        
        draw24thBox(color: inActiveColor)
        
        draw25thBox(color: inActiveColor)
        
        draw26thBox(color: inActiveColor)
        
        draw27thBox(color: inActiveColor)
        
        
        //refrig
        switch sliderVal {
            
        case 33:
            
            draw1stBox(color: UIColor.white)
            
        case 34:
            
            draw2ndBox(color : UIColor.white)
            
        case 35:
            
            draw3rdBox(color : UIColor.white)
            
        case 36:
            
            draw4thBox(color : UIColor.white)
            
        case 37:
            
            draw5thBox(color : UIColor.white)
            
        case 38:
            
            draw6thBox(color : UIColor.white)
            
        case 39:
            
            draw7thBox(color : UIColor.white)
            
        case 40:
            
            draw8thBox(color : UIColor.white)
            
        case 41:
            
            draw9thBox(color : UIColor.white)
            
        case 42:
            
            draw10thBox(color : UIColor.white)
            
        case 43:
            
            draw11thBox(color : UIColor.white)
            
        case 44:
            
            draw12thBox(color : UIColor.white)
            
        case 45:
            
            draw13thBox(color : UIColor.white)
            
        case 46:
            
            draw14thBox(color : UIColor.white)
            
            
            
        default: break
            
            //dont do anything
            
        }
        
        
        //freezer
        switch freezerVal {
            
        case -5:
            draw15thBox(color: UIColor.white)
        case -4:
            draw16thBox(color : UIColor.white)
        case -3:
            draw17thBox(color : UIColor.white)
        case -2:
            draw18thBox(color : UIColor.white)
        case -1:
            draw19thBox(color : UIColor.white)
        case 0:
            draw20thBox(color : UIColor.white)
        case 1:
            draw21thBox(color : UIColor.white)
        case 2:
            draw22thBox(color : UIColor.white)
        case 3:
            draw23thBox(color : UIColor.white)
        case 4:
            draw24thBox(color : UIColor.white)
        case 5:
            draw25thBox(color : UIColor.white)
        case 6:
            draw26thBox(color : UIColor.white)
        case 7:
            draw27thBox(color : UIColor.white)
            
        default: break
            //dont do anything
        }
        
        context?.setLineWidth(2.0)
        context?.setStrokeColor(UIColor.white.cgColor)
        context?.move(to: CGPoint(x: 20, y: 220))
        context?.addLine(to: CGPoint(x: 400, y: 220))
        context?.strokePath()
    }
    
    func drawBox(x: CGFloat, y: CGFloat, color : UIColor) {
        let temp46Path = UIBezierPath();
        temp46Path.move(to: CGPoint(x: 118.5, y: 21))
        temp46Path.addLine(to: CGPoint(x: 119, y: 31))
        temp46Path.addCurve(to: CGPoint(x: 121, y: 31), controlPoint1: CGPoint(x: 119.5, y: 30), controlPoint2: CGPoint(x: 120.5, y: 30))
        
        temp46Path.addLine(to: CGPoint(x: 121.5, y: 21))
        
        temp46Path.addCurve(to: CGPoint(x: 118.5, y: 21), controlPoint1: CGPoint(x: 121, y: 20), controlPoint2: CGPoint(x: 119, y: 20))
        
        temp46Path.close();
        
        UIColor.black.setStroke();
        
        temp46Path.stroke();
        
        color.setFill()
        
        temp46Path.fill()
    }
    
    //------------------------
    
    func draw8thBox(color : UIColor) {
        
        let temp46Path = UIBezierPath();
        
        temp46Path.move(to: CGPoint(x: 220, y: 40))
        
        temp46Path.addLine(to: CGPoint(x: 220, y: 70))
        
        temp46Path.addCurve(to: CGPoint(x: 235.68, y: 70.82), controlPoint1: CGPoint(x: 225.23, y: 70.1), controlPoint2: CGPoint(x: 232.56, y: 70.44))
        
        temp46Path.addLine(to: CGPoint(x: 238.82, y: 40.99))
        
        temp46Path.addCurve(to: CGPoint(x: 220, y: 40), controlPoint1: CGPoint(x: 226.28, y: 40.11), controlPoint2: CGPoint(x: 232.56, y: 40.44))
        
        temp46Path.close();
        
        UIColor.black.setStroke();
        
        temp46Path.stroke();
        
        color.setFill()
        
        temp46Path.fill()
        
        
        
    }
    
    
    
    func draw9thBox(color : UIColor) {
        
        let temp46Path = UIBezierPath();
        
        temp46Path.move(to: CGPoint(x: 245.05, y: 41.76))
        
        temp46Path.addLine(to: CGPoint(x: 240.87, y: 71.46))
        
        temp46Path.addCurve(to: CGPoint(x: 256.29, y: 74.46), controlPoint1: CGPoint(x: 246.05, y: 72.28), controlPoint2: CGPoint(x: 251.19, y: 73.28))
        
        temp46Path.addLine(to: CGPoint(x: 263.55, y: 45.25))
        
        temp46Path.addCurve(to: CGPoint(x: 245.05, y: 41.76), controlPoint1: CGPoint(x: 251.26, y: 42.73), controlPoint2: CGPoint(x: 257.42, y: 43.93))
        
        temp46Path.close();
        
        UIColor.black.setStroke();
        
        temp46Path.stroke();
        
        color.setFill()
        
        temp46Path.fill()
        
        
        
    }
    
    
    
    func draw10thBox(color : UIColor) {
        
        let temp46Path = UIBezierPath();
        
        temp46Path.move(to: CGPoint(x: 269.61, y: 46.97))
        
        temp46Path.addLine(to: CGPoint(x: 261.35, y: 75.81))
        
        temp46Path.addCurve(to: CGPoint(x: 276.19, y: 80.92), controlPoint1: CGPoint(x: 266.35, y: 77.34), controlPoint2: CGPoint(x: 271.30, y: 79.05))
        
        temp46Path.addLine(to: CGPoint(x: 287.43, y: 53.11))
        
        temp46Path.addCurve(to: CGPoint(x: 269.61, y: 46.97), controlPoint1: CGPoint(x: 281.56, y: 50.86), controlPoint2: CGPoint(x: 275.62, y: 48.81))
        
        temp46Path.close();
        
        UIColor.black.setStroke();
        
        temp46Path.stroke();
        
        color.setFill()
        
        temp46Path.fill()
        
        
        
    }
    
    
    
    func draw11thBox(color : UIColor) {
        
        let temp46Path = UIBezierPath();
        
        temp46Path.move(to: CGPoint(x: 293.21, y: 55.56))
        
        temp46Path.addLine(to: CGPoint(x: 281.01, y: 82.97))
        
        temp46Path.addCurve(to: CGPoint(x: 295, y: 90.1), controlPoint1: CGPoint(x: 285.76, y: 85.18), controlPoint2: CGPoint(x: 290.42, y: 87.56))
        
        temp46Path.addLine(to: CGPoint(x: 310, y: 64.12))
        
        temp46Path.addCurve(to: CGPoint(x: 293.21, y: 55.56), controlPoint1: CGPoint(x: 304.50, y: 61.07), controlPoint2: CGPoint(x: 298.90, y: 58.22))
        
        
        
        temp46Path.close();
        
        UIColor.black.setStroke();
        
        temp46Path.stroke();
        
        color.setFill()
        
        temp46Path.fill()
        
        
        
    }
    
    
    
    func draw12thBox(color : UIColor) {
        
        let temp46Path = UIBezierPath();
        
        temp46Path.move(to: CGPoint(x: 315.38, y: 67.35))
        
        temp46Path.addLine(to: CGPoint(x: 299.49, y: 92.8))
        
        temp46Path.addCurve(to: CGPoint(x: 312.35, y: 101.8), controlPoint1: CGPoint(x: 303.88, y: 95.64), controlPoint2: CGPoint(x: 308.17, y: 98.65))
        
        temp46Path.addLine(to: CGPoint(x: 330.82, y: 78.16))
        
        temp46Path.addCurve(to: CGPoint(x: 315.38, y: 67.35), controlPoint1: CGPoint(x: 325.8, y: 74.38), controlPoint2: CGPoint(x: 320.65, y: 70.77))
        
        
        
        temp46Path.close();
        
        UIColor.black.setStroke();
        
        temp46Path.stroke();
        
        color.setFill()
        
        temp46Path.fill()
        
        
        
    }
    
    
    
    func draw13thBox(color : UIColor) {
        
        let temp46Path = UIBezierPath();
        
        temp46Path.move(to: CGPoint(x: 335.7, y: 82.11))
        
        temp46Path.addLine(to: CGPoint(x: 316.42, y: 105.1))
        
        temp46Path.addCurve(to: CGPoint(x: 327.9, y: 115.8), controlPoint1: CGPoint(x: 320.37, y: 108.53), controlPoint2: CGPoint(x: 324.2, y: 112.1))
        
        temp46Path.addLine(to: CGPoint(x: 349.48, y: 94.96))
        
        temp46Path.addCurve(to: CGPoint(x: 335.7, y: 82.11), controlPoint1: CGPoint(x: 345.04, y: 90.52), controlPoint2: CGPoint(x: 340.44, y: 86.23))
        
        
        
        temp46Path.close();
        
        UIColor.black.setStroke();
        
        temp46Path.stroke();
        
        color.setFill()
        
        temp46Path.fill()
        
        
        
    }
    
    
    
    func draw14thBox(color : UIColor) {
        
        let temp46Path = UIBezierPath();
        
        
        
        temp46Path.move(to: CGPoint(x: 352.77, y: 99.56))
        
        temp46Path.addLine(to: CGPoint(x: 331.47, y: 119.63))
        
        temp46Path.addCurve(to: CGPoint(x: 341.35, y: 131.84), controlPoint1: CGPoint(x: 334.9, y: 123.58), controlPoint2: CGPoint(x: 338.20, y: 127.65))
        
        temp46Path.addLine(to: CGPoint(x: 365.62, y: 114.2))
        
        temp46Path.addCurve(to: CGPoint(x: 352.77, y: 99.56), controlPoint1: CGPoint(x: 361.84, y: 109.18), controlPoint2: CGPoint(x: 357.89, y: 104.3))
        
        
        
        temp46Path.close();
        
        UIColor.black.setStroke();
        
        temp46Path.stroke();
        
        color.setFill()
        
        temp46Path.fill()
        
        
        
    }
    
    
    
    func draw1stBox(color : UIColor) {
        
        let temp46Path = UIBezierPath();
        
        
        
        temp46Path.move(to: CGPoint(x: 70.78, y: 119.35))
        
        temp46Path.addLine(to: CGPoint(x: 95.64, y: 136.12))
        
        temp46Path.addCurve(to: CGPoint(x: 105.1, y: 123.58), controlPoint1: CGPoint(x: 98.65, y: 131.83), controlPoint2: CGPoint(x: 101.8, y: 127.65))
        
        temp46Path.addLine(to: CGPoint(x: 82.11, y: 104.3))
        
        temp46Path.addCurve(to: CGPoint(x: 70.78, y: 119.35), controlPoint1: CGPoint(x: 78.16, y: 109.18), controlPoint2: CGPoint(x: 74.38, y: 114.2))
        
        
        
        
        
        temp46Path.close();
        
        UIColor.black.setStroke();
        
        temp46Path.stroke();
        
        color.setFill()
        
        temp46Path.fill()
        
        
        
    }
    
    
    
    func draw2ndBox(color : UIColor) {
        
        let temp46Path = UIBezierPath();
        
        
        
        temp46Path.move(to: CGPoint(x: 86.23, y: 99.56))
        
        temp46Path.addLine(to: CGPoint(x: 108.53, y: 119.63))
        
        temp46Path.addCurve(to: CGPoint(x: 119.63, y: 108.53), controlPoint1: CGPoint(x: 112.1, y: 115.8), controlPoint2: CGPoint(x: 115.8, y: 112.1))
        
        temp46Path.addLine(to: CGPoint(x: 99.56, y: 86.23))
        
        temp46Path.addCurve(to: CGPoint(x: 86.23, y: 99.56), controlPoint1: CGPoint(x: 94.96, y: 90.52), controlPoint2: CGPoint(x: 90.52, y: 94.96))
        
        
        
        temp46Path.close();
        
        UIColor.black.setStroke();
        
        temp46Path.stroke();
        
        color.setFill()
        
        temp46Path.fill()
        
        
        
    }
    
    
    
    func draw3rdBox(color : UIColor) {
        
        let temp46Path = UIBezierPath();
        
        
        
        temp46Path.move(to: CGPoint(x: 104.3, y: 82.11))
        
        temp46Path.addLine(to: CGPoint(x: 123.58, y: 105.1))
        
        temp46Path.addCurve(to: CGPoint(x: 136.12, y: 95.64), controlPoint1: CGPoint(x: 127.65, y: 101.8), controlPoint2: CGPoint(x: 131.83, y: 98.65))
        
        temp46Path.addLine(to: CGPoint(x: 119.35, y: 70.77))
        
        temp46Path.addCurve(to: CGPoint(x: 104.3, y: 82.11), controlPoint1: CGPoint(x: 114.2, y: 74.38), controlPoint2: CGPoint(x: 109.18, y: 78.16))
        
        
        
        temp46Path.close();
        
        UIColor.black.setStroke();
        
        temp46Path.stroke();
        
        color.setFill()
        
        temp46Path.fill()
        
        
        
    }
    
    
    
    func draw4thBox(color : UIColor) {
        
        let temp46Path = UIBezierPath();
        
        
        
        temp46Path.move(to: CGPoint(x: 124.61, y: 67.35))
        
        temp46Path.addLine(to: CGPoint(x: 140.51, y: 92.79))
        
        temp46Path.addCurve(to: CGPoint(x: 154.24, y: 85.18), controlPoint1: CGPoint(x: 145, y: 90.1), controlPoint2: CGPoint(x: 149.58, y: 87.56))
        
        temp46Path.addLine(to: CGPoint(x: 141.1, y: 58.22))
        
        temp46Path.addCurve(to: CGPoint(x: 124.61, y: 67.35), controlPoint1: CGPoint(x: 135.5, y: 61.07), controlPoint2: CGPoint(x: 130, y: 64.12))
        
        
        
        temp46Path.close();
        
        UIColor.black.setStroke();
        
        temp46Path.stroke();
        
        color.setFill()
        
        temp46Path.fill()
        
        
        
    }
    
    
    
    func draw5thBox(color : UIColor) {
        
        let temp46Path = UIBezierPath();
        
        
        
        temp46Path.move(to: CGPoint(x: 146.79, y: 55.56))
        
        temp46Path.addLine(to: CGPoint(x: 158.99, y: 82.97))
        
        temp46Path.addCurve(to: CGPoint(x: 173.65, y: 77.34), controlPoint1: CGPoint(x: 163.81, y: 80.92), controlPoint2: CGPoint(x: 168.7, y: 79.05))
        
        temp46Path.addLine(to: CGPoint(x: 164.38, y: 48.81))
        
        temp46Path.addCurve(to: CGPoint(x: 146.79, y: 55.56), controlPoint1: CGPoint(x: 152.57, y: 53.11), controlPoint2: CGPoint(x: 158.44, y: 50.86))
        
        
        
        
        
        temp46Path.close();
        
        UIColor.black.setStroke();
        
        temp46Path.stroke();
        
        color.setFill()
        
        temp46Path.fill()
        
        
        
    }
    
    
    
    func draw6thBox(color : UIColor) {
        
        let temp46Path = UIBezierPath();
        
        
        
        temp46Path.move(to: CGPoint(x: 170.39, y: 46.97))
        
        temp46Path.addLine(to: CGPoint(x: 178.65, y: 75.8))
        
        temp46Path.addCurve(to: CGPoint(x: 193.95, y: 72.28), controlPoint1: CGPoint(x: 183.71, y: 74.45), controlPoint2: CGPoint(x: 188.81, y: 73.28))
        
        temp46Path.addLine(to: CGPoint(x: 188.74, y: 42.73))
        
        temp46Path.addCurve(to: CGPoint(x: 170.39, y: 46.97), controlPoint1: CGPoint(x: 182.58, y: 43.93), controlPoint2: CGPoint(x: 176.46, y: 45.35))
        
        
        
        temp46Path.close();
        
        UIColor.black.setStroke();
        
        temp46Path.stroke();
        
        color.setFill()
        
        temp46Path.fill()
        
        
        
    }
    
    
    
    func draw7thBox(color : UIColor) {
        
        let temp46Path = UIBezierPath();
        
        
        
        temp46Path.move(to: CGPoint(x: 194.95, y: 41.75))
        
        temp46Path.addLine(to: CGPoint(x: 199.12, y: 71.46))
        
        temp46Path.addCurve(to: CGPoint(x: 214.77, y: 70.1), controlPoint1: CGPoint(x: 204.32, y: 70.82), controlPoint2: CGPoint(x: 209.54, y: 70.37))
        
        temp46Path.addLine(to: CGPoint(x: 213.72, y: 40.11))
        
        temp46Path.addCurve(to: CGPoint(x: 194.95, y: 41.75), controlPoint1: CGPoint(x: 201.19, y: 40.99), controlPoint2: CGPoint(x: 207.44, y: 40.44))
        
        
        
        temp46Path.close();
        
        UIColor.black.setStroke();
        
        temp46Path.stroke();
        
        color.setFill()
        
        temp46Path.fill()
        
        
        
    }
    

    
    
    //-------------------------
        
    
    
    
    
    //// Freezer
    
    
    
    
    
    func draw21thBox(color : UIColor) {
        
        let temp46Path = UIBezierPath();
        
        temp46Path.move(to: CGPoint(x: 212.15, y: 369.79))
        temp46Path.addLine(to: CGPoint(x: 210.58, y: 399.75))
        temp46Path.addCurve(to: CGPoint(x: 229.42, y: 399.75), controlPoint1: CGPoint(x: 216.86, y: 399.97), controlPoint2: CGPoint(x: 223.14, y: 399.97))
        temp46Path.addLine(to: CGPoint(x: 227.85, y: 369.79))
        temp46Path.addCurve(to: CGPoint(x: 212.15, y: 369.79), controlPoint1: CGPoint(x: 222.62, y: 369.95), controlPoint2: CGPoint(x: 217.38, y: 369.95))

        temp46Path.close();
        UIColor.black.setStroke();
        temp46Path.stroke();
        color.setFill()
        temp46Path.fill()
    }
    
    
    
    func draw20thBox(color : UIColor) {
        
        let temp46Path = UIBezierPath();
        
        temp46Path.move(to: CGPoint(x: 191.38, y: 367.24))
        temp46Path.addLine(to: CGPoint(x: 185.65, y: 396.69))
        temp46Path.addCurve(to: CGPoint(x: 204.31, y: 399.32), controlPoint1: CGPoint(x: 191.84, y: 397.78), controlPoint2: CGPoint(x: 198.06, y: 398.66))
        temp46Path.addLine(to: CGPoint(x: 206.93, y: 369.43))
        temp46Path.addCurve(to: CGPoint(x: 191.38, y: 367.24), controlPoint1: CGPoint(x: 201.72, y: 368.88), controlPoint2: CGPoint(x: 196.53, y: 368.15))
        
        temp46Path.close();
        UIColor.black.setStroke();
        temp46Path.stroke();
        color.setFill()
        temp46Path.fill()                
    }
    
    
    
    func draw19thBox(color : UIColor) {
        
        let temp46Path = UIBezierPath();
        
        temp46Path.move(to: CGPoint(x: 171.17, y: 361.83))
        temp46Path.addLine(to: CGPoint(x: 161.4, y: 390.19))
        temp46Path.addCurve(to: CGPoint(x: 179.51, y: 395.39), controlPoint1: CGPoint(x: 167.37, y: 392.13), controlPoint2: CGPoint(x: 173.41, y: 393.87))
        temp46Path.addLine(to: CGPoint(x: 186.26, y: 366.16))
        temp46Path.addCurve(to: CGPoint(x: 171.17, y: 361.83), controlPoint1: CGPoint(x: 181.18, y: 364.89), controlPoint2: CGPoint(x: 176.14, y: 363.45))
        temp46Path.close();
        
        UIColor.black.setStroke();
        temp46Path.stroke();
        color.setFill()
        temp46Path.fill()
    }
    
    
    
    func draw18thBox(color : UIColor) {
        
        let temp46Path = UIBezierPath();
        
        temp46Path.move(to: CGPoint(x: 151.9, y: 353.65))
        temp46Path.addLine(to: CGPoint(x: 138.28, y: 380.38))
        temp46Path.addCurve(to: CGPoint(x: 155.49, y: 388.04), controlPoint1: CGPoint(x: 143.93, y: 383.14), controlPoint2: CGPoint(x: 149.67, y: 385.7))
        temp46Path.addLine(to: CGPoint(x: 166.24, y: 360.04))
        temp46Path.addCurve(to: CGPoint(x: 151.9, y: 353.65), controlPoint1: CGPoint(x: 161.39, y: 358.08), controlPoint2: CGPoint(x: 156.6, y: 355.95))
        temp46Path.close();
        
        UIColor.black.setStroke();
        
        temp46Path.stroke();
        
        color.setFill()
        
        temp46Path.fill()
        
        
        
    }
    
    
    
    func draw17thBox(color : UIColor) {
        
        let temp46Path = UIBezierPath();
        
        temp46Path.move(to: CGPoint(x: 133.96, y: 342.87))
        temp46Path.addLine(to: CGPoint(x: 116.76, y: 367.45))
        temp46Path.addCurve(to: CGPoint(x: 132.73, y: 377.43), controlPoint1: CGPoint(x: 121.96, y: 370.96), controlPoint2: CGPoint(x: 127.29, y: 374.29))
        temp46Path.addLine(to: CGPoint(x: 147.28, y: 351.19))
        temp46Path.addCurve(to: CGPoint(x: 133.96, y: 342.87), controlPoint1: CGPoint(x: 142.74, y: 348.58), controlPoint2: CGPoint(x: 138.3, y: 345.8))
        
        temp46Path.close();
        UIColor.black.setStroke();
        temp46Path.stroke();
        color.setFill()
        temp46Path.fill()
        
        
        
    }
    
    
    
    func draw16thBox(color : UIColor) {
        
        let temp46Path = UIBezierPath();
        
        temp46Path.move(to: CGPoint(x: 117.77, y: 329.7))
        temp46Path.addLine(to: CGPoint(x: 97.24, y: 351.64))
        temp46Path.addCurve(to: CGPoint(x: 111.67, y: 363.75), controlPoint1: CGPoint(x: 101.91, y: 355.85), controlPoint2: CGPoint(x: 106.72, y: 359.89))
        temp46Path.addLine(to: CGPoint(x: 129.73, y: 339.8))
        temp46Path.addCurve(to: CGPoint(x: 117.77, y: 329.7), controlPoint1: CGPoint(x: 125.6, y: 336.57), controlPoint2: CGPoint(x: 121.59, y: 333.21))
        temp46Path.close();
        
        UIColor.black.setStroke();
        
        temp46Path.stroke();
        
        color.setFill()
        
        temp46Path.fill()
        
        
        
    }
    
    
    
    func draw15thBox(color : UIColor) {
        
        let temp46Path = UIBezierPath();
        
        temp46Path.move(to: CGPoint(x: 103.43, y: 314.4))
        temp46Path.addLine(to: CGPoint(x: 80.11, y: 333.28))
        temp46Path.addCurve(to: CGPoint(x: 92.72, y: 347.28), controlPoint1: CGPoint(x: 84.15, y: 338.09), controlPoint2: CGPoint(x: 88.36, y: 342.76))
        temp46Path.addLine(to: CGPoint(x: 113.93, y: 326.07))
        temp46Path.addCurve(to: CGPoint(x: 103.43, y: 314.4), controlPoint1: CGPoint(x: 110.3, y: 322.3), controlPoint2: CGPoint(x: 106.79, y: 318.4))
        temp46Path.close();
        
        UIColor.black.setStroke();
        
        temp46Path.stroke();
        
        color.setFill()
        
        temp46Path.fill()
        
        
        
    }
    
    
    
    func draw22thBox(color : UIColor) {
        
        let temp46Path = UIBezierPath();
        
        temp46Path.move(to: CGPoint(x: 233.07, y: 369.43))
        temp46Path.addLine(to: CGPoint(x: 235.69, y: 399.32))
        temp46Path.addCurve(to: CGPoint(x: 254.35, y: 396.69), controlPoint1: CGPoint(x: 241.94, y: 398.66), controlPoint2: CGPoint(x: 248.16, y: 397.78))
        temp46Path.addLine(to: CGPoint(x: 248.62, y: 367.24))
        temp46Path.addCurve(to: CGPoint(x: 233.07, y: 369.43), controlPoint1: CGPoint(x: 243.47, y: 368.15), controlPoint2: CGPoint(x: 238.28, y: 368.88))

        temp46Path.close();
        
        UIColor.black.setStroke();
        
        temp46Path.stroke();
        
        color.setFill()
        
        temp46Path.fill()
        
        
        
    }
    
    
    
    func draw23thBox(color : UIColor) {
        
        let temp46Path = UIBezierPath();
        
        temp46Path.move(to: CGPoint(x: 253.74, y: 366.16))
        temp46Path.addLine(to: CGPoint(x: 260.49, y: 395.39))
        temp46Path.addCurve(to: CGPoint(x: 278.6, y: 390.19), controlPoint1: CGPoint(x: 266.59, y: 393.89), controlPoint2: CGPoint(x: 272.63, y: 392.13))
        temp46Path.addLine(to: CGPoint(x: 268.84, y: 361.83))
        temp46Path.addCurve(to: CGPoint(x: 253.74, y: 366.16), controlPoint1: CGPoint(x: 263.86, y: 363.45), controlPoint2: CGPoint(x: 258.82, y: 364.89))

        temp46Path.close();
        
        UIColor.black.setStroke();
        
        temp46Path.stroke();
        
        color.setFill()
        
        temp46Path.fill()
        
        
        
    }
    
    
    
    func draw24thBox(color : UIColor) {
        
        let temp46Path = UIBezierPath();
        
        temp46Path.move(to: CGPoint(x: 273.76, y: 360.04))
        temp46Path.addLine(to: CGPoint(x: 284.51, y: 388.04))
        temp46Path.addCurve(to: CGPoint(x: 301.72, y: 380.38), controlPoint1: CGPoint(x: 290.33, y: 385.69), controlPoint2: CGPoint(x: 296.07, y: 383.14))
        temp46Path.addLine(to: CGPoint(x: 288.1, y: 353.65))
        temp46Path.addCurve(to: CGPoint(x: 273.76, y: 360.04), controlPoint1: CGPoint(x: 283.39, y: 355.95), controlPoint2: CGPoint(x: 278.61, y: 358.08))
        
        temp46Path.close();
        
        UIColor.black.setStroke();
        
        temp46Path.stroke();
        
        color.setFill()
        
        temp46Path.fill()
        
        
        
    }
    
    
    
    func draw25thBox(color : UIColor) {
        
        let temp46Path = UIBezierPath();
        
        temp46Path.move(to: CGPoint(x: 292.72, y: 351.19))
        temp46Path.addLine(to: CGPoint(x: 307.27, y: 377.43))
        temp46Path.addCurve(to: CGPoint(x: 323.24, y: 367.45), controlPoint1: CGPoint(x: 312.71, y: 374.29), controlPoint2: CGPoint(x: 318.04, y: 370.96))
        temp46Path.addLine(to: CGPoint(x: 306.04, y: 343.88))
        temp46Path.addCurve(to: CGPoint(x: 292.72, y: 351.19), controlPoint1: CGPoint(x: 301.7, y: 345.8), controlPoint2: CGPoint(x: 297.26, y: 348.58))
        
        temp46Path.close();
        
        UIColor.black.setStroke();
        
        temp46Path.stroke();
        
        color.setFill()
        
        temp46Path.fill()
        
        
        
    }
    
    
    
    func draw26thBox(color : UIColor) {
        
        let temp46Path = UIBezierPath();
        
        temp46Path.move(to: CGPoint(x: 310.27, y: 339.8))
        temp46Path.addLine(to: CGPoint(x: 328.33, y: 363.75))
        temp46Path.addCurve(to: CGPoint(x: 342.76, y: 351.64), controlPoint1: CGPoint(x: 333.28, y: 359.89), controlPoint2: CGPoint(x: 338.1, y: 355.85))
        temp46Path.addLine(to: CGPoint(x: 322.3, y: 329.7))
        temp46Path.addCurve(to: CGPoint(x: 310.27, y: 339.8), controlPoint1: CGPoint(x: 318.4, y: 333.21), controlPoint2: CGPoint(x: 314.4, y: 336.57))
        
        temp46Path.close();
        
        UIColor.black.setStroke();
        
        temp46Path.stroke();
        
        color.setFill()
        
        temp46Path.fill()
        
        
        
    }
    
    
    
    func draw27thBox(color : UIColor) {
        
        let temp46Path = UIBezierPath();
        
        temp46Path.move(to: CGPoint(x: 326.07, y: 326.07))
        temp46Path.addLine(to: CGPoint(x: 347.28, y: 347.28))
        temp46Path.addCurve(to: CGPoint(x: 359.89, y: 333.28), controlPoint1: CGPoint(x: 351.64, y: 342.76), controlPoint2: CGPoint(x: 355.84, y: 338.09))
        temp46Path.addLine(to: CGPoint(x: 336.57, y: 314.4))
        temp46Path.addCurve(to: CGPoint(x: 326.07, y: 326.07), controlPoint1: CGPoint(x: 333.21, y: 318.41), controlPoint2: CGPoint(x: 329.7, y: 332.3))
        
        temp46Path.close();
        
        UIColor.black.setStroke();
        
        temp46Path.stroke();
        
        color.setFill()
        
        temp46Path.fill()
        
        
        
    }
    
    
    
}


//
//  WasherSettingViewController.swift
//  ipadDemo
//
//  Created by Mahajan, Nayanank (Contractor) on 5/18/17.
//  Copyright © 2017 Mahajan, Nayanank (Contractor). All rights reserved.
//

import UIKit

class WasherSettingViewController: UIViewController {

    @IBOutlet weak var chimeLevelImageWasher: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func chimeOff(_ sender: UIButton) {
        chimeLevelImageWasher.image = #imageLiteral(resourceName: "chime0")
    }
    
    @IBAction func chimeMedium(_ sender: UIButton) {
        chimeLevelImageWasher.image = #imageLiteral(resourceName: "chime1")
    }
    
    @IBAction func chimeHigh(_ sender: UIButton) {
        chimeLevelImageWasher.image = #imageLiteral(resourceName: "chime3")
    }
    
}
